import React from "react";
import AutoComplete from "../../component/Autocomplte";
import ViewWeathersStats from "../ViewWeathersStats";
import LoadingSpinner from "../../component/loadingSpinner";

const appID = "f825344b0cf0672c689378549f9868db";
function Weather() {
  const [loading, setLoading] = React.useState(false);
  const [weather, setWeather] = React.useState(undefined);

  const changeText = async (obj) => {
    setLoading(true);
    try {
      const res = await fetch(
        `https://api.openweathermap.org/data/2.5/weather?q=${obj.text}&appid=${appID}`,
        {
          method: "get",
        }
      );
      const response = await res.json();
      setWeather(response);
      setLoading(false);
    } catch (err) {
      setLoading(false);
      setWeather(undefined);
    }
  };

  return (
    <div className="App">
      <AutoComplete submitKeyword={(keyword)=>changeText({text: keyword})} changeText={changeText} />
      {loading ? <LoadingSpinner /> :
      weather !== undefined ? (
        <div>
          <ViewWeathersStats data={weather} />
        </div>
      ) : null}
    </div>
  );
}

export default Weather;
