import React, { useState } from "react";

function Autocomplete(props) {
  const [activeSuggestion, setActiveSuggestion] = useState(0);
  const [filteredSuggestions, setFlteredSuggestions] = useState(0);
  const [showSuggestions, setShowSuggestions] = useState(0);
  const [userInput, setUserInput] = useState("");
  const [myTimeOutId, setmyTimeOutId] = useState(null);
  const [clicked, setClicked] = useState(false);

  const onChange = (e) => {
    const userInput = e.currentTarget.value;
    setUserInput(e.currentTarget.value);
    setActiveSuggestion(0);
    setClicked(false);
    setFlteredSuggestions([]);
    setShowSuggestions(false);

    clearTimeout(myTimeOutId);
    let t = setTimeout(() => {
      if (userInput.length > 0) {
        getData(userInput);
      }
    }, 1000);
    setmyTimeOutId(t);
  };

  const getData = async (userInput) => {
    try {
      const res = await fetch(
        `http://api.positionstack.com/v1/forward?access_key=a10a9090bc19ecf01d39a0383d2911a4&query=${userInput}`,
        {
          method: "get",
        }
      );
      const response = await res.json();
      setActiveSuggestion(response.data.length);
      setFlteredSuggestions(response.data);
      setShowSuggestions(true);
    } catch (err) {
      setActiveSuggestion(0);
      setFlteredSuggestions([]);
      setShowSuggestions(false);
    }
  };

  const onClick = (e, locality, name) => {
    setActiveSuggestion(0);
    setFlteredSuggestions([]);
    setShowSuggestions(false);
    setClicked(true);
    setUserInput(e.currentTarget.innerText);
    props.changeText({ text: e.currentTarget.innerText, locality, name });
  };

  const onKeyDown = (e) => {
    setClicked(false);
    if (e.keyCode === 13) {
      setActiveSuggestion(0);
      setShowSuggestions(false);
      
      setUserInput(filteredSuggestions[activeSuggestion]);
    } else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }
      setActiveSuggestion(activeSuggestion - 1);
    }
    // User pressed the down arrow, increment the index
    else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }
      setActiveSuggestion(activeSuggestion + 1);
    }
  };

  let suggestionsListComponent;

  if (showSuggestions && userInput) {
    if (filteredSuggestions.length) {
      suggestionsListComponent = (
        <ul className="suggestions">
          {filteredSuggestions.map((suggestion, index) => {
            let className;

            // Flag the active suggestion with a class
            if (index === activeSuggestion) {
              className = "suggestion-active";
            }
            return (
              <li
                className={className}
                key={index}
                onClick={(e) =>
                  onClick(e, suggestion.locality, suggestion.name)
                }
              >
                {suggestion.label}
              </li>
            );
          })}
        </ul>
      );
    } else {
      suggestionsListComponent = (
        <div className="no-suggestions">
          <em>No suggestions available.</em>
        </div>
      );
    }
  }

  return (
    <React.Fragment>
      <div
        style={{
          display: "flex",
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <input
          type="text"
          placeholder="Search for locality"
          onChange={onChange}
          onKeyDown={onKeyDown}
          value={userInput}
        />
        {suggestionsListComponent}
      </div>
      <div>
        {userInput.length > 0 && !clicked && (
          <button onClick={()=>props.submitKeyword(userInput)}>Click here to search just for keyword {userInput}</button>
        )}
      </div>
    </React.Fragment>
  );
}

export default Autocomplete;
