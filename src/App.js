import './App.css';
import Weather from './modules/Weather'

function App() {
  return (
    <div className="App">
      <Weather />
    </div>
  );
}

export default App;
